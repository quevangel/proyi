#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>

#include <math.h>

struct vector
{
	double x, y, z;
};

struct vector project(struct vector v)
{
	struct vector p;
	p.x = v.x / v.z;
	p.y = v.y / v.z;
	p.z = 1.0;
	return p;
}

void set_triangle(struct vector points[])
{
	points[0] = (struct vector){x: 0, y: 1, z: 0};
	points[1] = (struct vector){x: 1, y: -1, z: 0};
	points[2] = (struct vector){x: -1, y: -1, z: 0};
}

struct vector sum(struct vector v, struct vector w)
{
	return (struct vector){
		x: v.x + w.x,
		y: v.y + w.y,
		z: v.z + w.z};
}

void add_to_all(struct vector v[], unsigned n, struct vector w)
{
	for(unsigned i = 0; i < n; i++)
	{
		v[i] = sum(v[i], w);
	}
}

int main(int argc, char* argv[]) 
{
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	int app_alive = 0;
	const int no_points = 3;
	struct vector center = {x: 0, y: 0, z: 2};
	struct vector points[no_points];
	unsigned time = 0;

	set_triangle(points);
	add_to_all(points, no_points, center);

	SDL_Init(SDL_INIT_VIDEO);

	window = SDL_CreateWindow("proyi",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
			800, 800,
			0);
	app_alive = (window != NULL);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	while(app_alive)
	{
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		time++;

		double divisor = 10000.0;
		double stime = time / divisor;

		center.z = 2 + sin(stime);
		center.x = 0 + cos(stime);

		set_triangle(points);
		add_to_all(points, no_points, center);

		struct vector projected_points[no_points];
		for(unsigned i = 0u; i < no_points; i++)
		{
			projected_points[i] = project(points[i]);
		}

		for(unsigned i = 0u; i < no_points; i++)
		{
			unsigned ni = (i + 1) % no_points;
			int x, y;
			int nx, ny;
			double px, py;
			double npx, npy;

			px = projected_points[i].x;
			py = projected_points[i].y;
			
			npx = projected_points[ni].x;
			npy = projected_points[ni].y;

			x = (int)(400.0 + px * 400.0);
			y = (int)(400.0 - py * 400.0);

			nx = (int)(400.0 + npx * 400.0);
			ny = (int)(400.0 - npy * 400.0);

			lineColor(renderer, x, y, nx, ny, 0xff0000ff);
		}
		SDL_RenderPresent(renderer);

		SDL_Event event;
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_QUIT:
					app_alive = 0;
					break;
				default:
					break;
			}
		}
	}

	return 0;
}
